using UnityEngine;

namespace Infrastracture
{
    public class GameBootsTrapper : MonoBehaviour
    {
        private Game _game;
        private void Awake()
        {
            _game = new Game();
            
            DontDestroyOnLoad(gameObject);
        }
    }
}