﻿using UnityEngine;

namespace Services.Input
{
    public interface IInputService
    {
        Vector2 Axis { get; }
        float TowerRotation { get; }

        bool IsAtackbuttonUp();

        bool IsChangeWeaponLeftUp();

        bool IsChangeWeaponRightUp();
    }
}