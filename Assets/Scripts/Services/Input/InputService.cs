﻿using UnityEngine;

namespace Services.Input
{
    class InputService : IInputService
    {
        private const string Horisontal = "Horizontal";
        private const string Vertical = "Vertical";
        private const string ButtonFire = "Fire";
        private const string TowerAxis = "TowerAxis";
        private const string ArsenalLeft = "ArsenalLeft";
        private const string ArsenalRight = "ArsenalRight";

        public Vector2 Axis =>
            new Vector2(UnityEngine.Input.GetAxis(Horisontal),
                UnityEngine.Input.GetAxis(Vertical));

        public float TowerRotation
        {
            get => UnityEngine.Input.GetAxis(TowerAxis);
        }

        public bool IsAtackbuttonUp() => UnityEngine.Input.GetButton(ButtonFire);
        public bool IsChangeWeaponLeftUp()=> UnityEngine.Input.GetButton(ArsenalLeft);

        public bool IsChangeWeaponRightUp()=> UnityEngine.Input.GetButton(ArsenalRight);
    }
}