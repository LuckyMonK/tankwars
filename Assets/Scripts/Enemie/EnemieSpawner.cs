using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemie
{
    public class EnemieSpawner : MonoBehaviour
    {
        [SerializeField] private Transform[] spawnPlaces;

        [SerializeField] private EnemieData[] enemies;
        [Min(0)] [SerializeField] private int numberInStack;

        private List<EnemieData> _currentEnemies = new List<EnemieData>();
        [SerializeField] private Transform followTarget;
    
        private void Start()
        {
            Spawn();
        }

        private void Spawn()
        {
            for (int i = _currentEnemies.Count; i < numberInStack; i++)
            {
                var enemie = Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Length)],
                    spawnPlaces[UnityEngine.Random.Range(0, spawnPlaces.Length)].position,
                    Quaternion.identity);

                enemie.deathEvent.AddListener(() => { RemoveEnemieFromStack(enemie); });
                enemie.deathEvent.AddListener(Spawn);
                enemie.UpdateFollowTarget(followTarget);
                _currentEnemies.Add(enemie);
            }
        }

        private void RemoveEnemieFromStack(EnemieData enemie)
        {
            _currentEnemies.Remove(enemie);
        }
    }
}