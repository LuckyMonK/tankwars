using Player;
using UnityEngine;
using UnityEngine.Events;

namespace Enemie
{
    public class EnemieData : MonoBehaviour, IDamageable, IDangerous
    {
        [SerializeField] private float health;
        [Range(0, 100)] [SerializeField] private int armor;
        [SerializeField] private int damage;
        [SerializeField] private float damageCooldown;
        [Min(0)] public float speed;
        public UnityEvent deathEvent;
        private bool _isWaiting = false;
        private Transform _followTarget;

        public void TakingDamage(int damage)
        {
            health -= damage * (1f - armor / 100f);
            if (health <= 0)
            {
                Death();
            }
        }

        public void Death()
        {
            deathEvent.Invoke();
            Destroy(gameObject);
        }

        public TargetType GetTargetType()
        {
            return TargetType.Enemie;
        }

        public void InflictDamage(IDamageable target, int damage)
        {
            target.TakingDamage(damage);
        }

        private void OnCollisionStay(Collision other)
        {
            IDamageable target = other.gameObject.GetComponent<IDamageable>();
            if (target != null && !_isWaiting)
            {
                if (target.GetTargetType() != GetTargetType())
                {
                    _isWaiting = true;
                    InflictDamage(target, damage);
                    Invoke("StopWaiting", damageCooldown);
                }
            }
        }
        
        private void StopWaiting() => _isWaiting = false;

        public void UpdateFollowTarget(Transform newTarget) => _followTarget = newTarget;

        public Transform GetFollowTarget() => _followTarget;
    }
}