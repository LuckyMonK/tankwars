using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;

namespace Enemie
{
    public class EnemieAI : MonoBehaviour
    {
        private const float MinimumDistance = 0.2f;
        [SerializeField] private NavMeshAgent agent;
        private Transform _target;
        [SerializeField] private EnemieData data;
        private void Start()
        {
            agent.speed = data.speed;
            _target = data.GetFollowTarget();
        }

        private void Update()
        {
            if (Vector3.Distance(_target.position, agent.transform.position) > MinimumDistance)
            {
                agent.destination = _target.transform.position;
            }
        }
    }
}