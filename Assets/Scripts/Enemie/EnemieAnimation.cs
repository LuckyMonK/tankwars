using UnityEngine;
using UnityEngine.Serialization;

namespace Enemie
{
    public class EnemieAnimation : MonoBehaviour
    {
        private static readonly int Dead = Animator.StringToHash("Dead");
        private static readonly int Move = Animator.StringToHash("Move");
        private static readonly int Blend = Animator.StringToHash("Blend");
        [SerializeField] private Animator animator;

        public void PlayDeath() => animator.SetTrigger(Dead);

        public void Moving(float speed)
        {
            animator.SetBool(Move, true);
            animator.SetFloat(Blend, speed);
        }

        public void StopMoving()
        {
            animator.SetBool(Move, false);
        }
    }
}