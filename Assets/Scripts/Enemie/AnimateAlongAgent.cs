using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;

namespace Enemie
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(EnemieAnimation))]
    public class AnimateAlongAgent : MonoBehaviour
    {
        private const float MinimumVelocity = 0.1f;
        [SerializeField] private NavMeshAgent agent;
        [SerializeField] private EnemieAnimation enemieAnimation;

        private void Update()
        {
            if (ShouldMove())
            {
                enemieAnimation.Moving(agent.velocity.magnitude);
            }
            else
            {
                enemieAnimation.StopMoving();
            }
        }

        private bool ShouldMove() =>
            agent.velocity.magnitude > MinimumVelocity && agent.remainingDistance > agent.radius;
    }
}
