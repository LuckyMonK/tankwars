using System;
using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private AnimationCurve[] positionCurves;
    [SerializeField] private float timeOut = 100f;

    private IDamageable _source;

    private int _damage;

    public void CreateProjectile(AnimationCurve[] curves, IDamageable source, int damage)
    {
        positionCurves = curves;
        this._source = source;
        this._damage = damage;
        StartCoroutine(BulletFlight());
    }


    private IEnumerator BulletFlight()
    {
        float timer = 0f;
        Vector3 startPosition = transform.position;
        while (timer < timeOut)
        {
            transform.position = startPosition
                                 + transform.forward * (timer * speed)
                                 + new Vector3(positionCurves[0].Evaluate(timer % 1f),
                                     positionCurves[1].Evaluate(timer % 1f),
                                     0);
            timer += Time.deltaTime;
            yield return null;
        }

        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision other)
    {
        IDamageable target = other.gameObject.GetComponent<IDamageable>();
        if (target != null)
        {
            if (target != _source)
            {
                target.TakingDamage(_damage);
                Destroy(gameObject);
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }
}