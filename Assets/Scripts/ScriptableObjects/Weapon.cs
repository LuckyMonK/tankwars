using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Weapon", menuName = "ScriptableObjects/Weapon", order = 1)]
public class Weapon : ScriptableObject
{
    public int damage;
    public Projectile projectile;
    public AnimationCurve[] trajectorys;        //задает траекторию полета по x, y
    public GameObject weaponView;
}
