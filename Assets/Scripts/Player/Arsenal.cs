using System;
using System.Collections;
using System.Collections.Generic;
using Infrastracture;
using Services.Input;
using UnityEngine;
using UnityEngine.Serialization;

namespace Player
{
    public class Arsenal : MonoBehaviour
    {
        [SerializeField] private List<Weapon> weapons;
        private int _actualWeapon;
        private IInputService _inputService;

        private bool _recharge = false;

        [SerializeField] private Transform weaponViewPosition;
        private GameObject _actualWeaponView;
        [SerializeField] private Transform shootingPosition;
        private IDamageable _damageable;

        private void Awake()
        {
            _inputService = Game.InputService;
            ChangeWeaponView();
            _damageable = GetComponent<IDamageable>();
        }

        private void Update()
        {
            if (_inputService.IsChangeWeaponLeftUp() && !_recharge)
            {
                ChangeWeapon(-1);
            }

            if (_inputService.IsChangeWeaponRightUp() && !_recharge)
            {
                ChangeWeapon(1);
            }
        }

        private void ChangeWeapon(int indexDirection)
        {
            _recharge = true;
            int tempCount = _actualWeapon + indexDirection;

            _actualWeapon = tempCount < 0
                ? (weapons.Count - 1)
                : (tempCount > (weapons.Count - 1) ? 0 : tempCount);

            ChangeWeaponView();
            //заглушка, чтобы пользователь мог адекватно воспринимать прокрутку оружия
            Invoke("StopBlock", 0.3f);
        }

        private void ChangeWeaponView()
        {
            Destroy(_actualWeaponView);
            _actualWeaponView = Instantiate(GetActualWeapon().weaponView, weaponViewPosition);
        }

        private void StopBlock() => _recharge = false;

        public Weapon GetActualWeapon()
        {
            return weapons[_actualWeapon];
        }

        public void GenerateProjectile(int damage)
        {
            var actualWeapon = GetActualWeapon();
            var proj = Instantiate(actualWeapon.projectile,
                shootingPosition.position,
                Quaternion.LookRotation(shootingPosition.forward));

            proj.CreateProjectile(actualWeapon.trajectorys, _damageable,
                actualWeapon.damage + damage);
        }
    }
}