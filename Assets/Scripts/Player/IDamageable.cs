namespace Player
{
    public interface IDamageable
    {
        public void TakingDamage(int damage);
        public void Death();

        public TargetType GetTargetType();
    }
}
