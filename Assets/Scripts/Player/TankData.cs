﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Player
{
    public class TankData : MonoBehaviour, IDamageable
    {
        [Range(0, 100)] [SerializeField] private float health;

        [Range(0, 100)] [SerializeField] private int armor;

        public void TakingDamage(int damage)
        {
            health -= damage * (1f - armor / 100f);
            if (health <= 0)
            {
                Death();
            }
        }

        public void Death()
        {
            SceneManager.LoadScene(sceneBuildIndex: 0);
        }

        public TargetType GetTargetType()
        {
            return TargetType.Player;
        }
    }
}