using Infrastracture;
using Services.Input;
using UnityEngine;
using UnityEngine.Serialization;

namespace Player
{
    public class CharacterMove : MonoBehaviour
    {
        public CharacterController characterController;
        private IInputService _inputService;
        private Camera _camera;
        public float movementSpeed;
        public float rotationSpeed;

        private void Awake()
        {
            _inputService = Game.InputService;
            _camera = Camera.main;
        }

        private void Update()
        {
            Vector3 movementVector = Vector3.zero;
            if (_inputService.Axis.sqrMagnitude > Constants.MovementEps)
            {
                movementVector = _camera.transform.TransformDirection(_inputService.Axis);
                movementVector.y = 0;
                movementVector.Normalize();

                transform.forward = Vector3.Lerp(transform.forward, movementVector,
                    Time.deltaTime * rotationSpeed);
            }

            movementVector += Physics.gravity;

            characterController.Move(movementVector 
                                     * (movementSpeed * Time.deltaTime));
        }
    }
}