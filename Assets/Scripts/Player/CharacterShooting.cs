﻿using Infrastracture;
using Services.Input;
using UnityEngine;
using UnityEngine.Serialization;

namespace Player
{
    public class CharacterShooting : MonoBehaviour
    {
        private IInputService _inputService;
        private bool _recharge = false;
        [SerializeField] private float cooldown;
        [SerializeField] private Arsenal arsenal;
        [SerializeField] private int defaultDamage; //урон разделен на урон от самого танка и оружия

        private void Awake()
        {
            _inputService = Game.InputService;
        }

        private void Update()
        {
            if (_inputService.IsAtackbuttonUp() && !_recharge)
            {
                Shoot();
            }
        }

        private void Shoot()
        {
            _recharge = true;
            arsenal.GenerateProjectile(defaultDamage);

            Invoke("Reload", cooldown);
        }

        private void Reload()
        {
            _recharge = false;
        }
    }
}