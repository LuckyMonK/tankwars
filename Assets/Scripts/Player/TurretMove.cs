using System;
using System.Collections;
using System.Collections.Generic;
using Infrastracture;
using Services.Input;
using UnityEngine;
using UnityEngine.Serialization;

namespace Player
{
    public class TurretMove : MonoBehaviour
    {
        private IInputService _inputService;
        [SerializeField] private float rotationSpeed;

        private void Awake()
        {
            _inputService = Game.InputService;
        }

        private void Update()
        {
            if (Math.Abs(_inputService.TowerRotation) > Constants.TowerMovementEps)
            {
                transform.Rotate(new Vector3(0, 0, _inputService.TowerRotation) 
                                 * (Time.deltaTime * rotationSpeed));
            }
        }
    }
}