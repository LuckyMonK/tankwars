﻿namespace Player
{
    public interface IDangerous
    {
        void InflictDamage(IDamageable target, int damage);
    }
}